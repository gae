package com.google.appengine.api.datastore;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.datastore.v1beta3.client.DatastoreHelper.LOCAL_HOST_ENV_VAR;
import static com.google.datastore.v1beta3.client.DatastoreHelper.PRIVATE_KEY_FILE_ENV_VAR;
import static com.google.datastore.v1beta3.client.DatastoreHelper.PROJECT_ID_ENV_VAR;
import static com.google.datastore.v1beta3.client.DatastoreHelper.SERVICE_ACCOUNT_ENV_VAR;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.appengine.api.datastore.DatastoreServiceConfig.ApiVersion;
import com.google.appengine.api.taskqueue.TaskQueuePb.TaskQueueAddRequest;
import com.google.appengine.api.taskqueue.TaskQueuePb.TaskQueueBulkAddRequest;
import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.apphosting.api.ApiProxy.ApiProxyException;
import com.google.apphosting.api.ApiProxy.Delegate;
import com.google.apphosting.api.ApiProxy.Environment;
import com.google.apphosting.api.ApiProxy.EnvironmentFactory;
import com.google.apphosting.api.ApiProxy.LogRecord;
import com.google.datastore.v1beta3.AllocateIdsRequest;
import com.google.datastore.v1beta3.AllocateIdsResponse;
import com.google.datastore.v1beta3.BeginTransactionRequest;
import com.google.datastore.v1beta3.BeginTransactionResponse;
import com.google.datastore.v1beta3.CommitRequest;
import com.google.datastore.v1beta3.CommitResponse;
import com.google.datastore.v1beta3.Datastore.Method;
import com.google.datastore.v1beta3.LookupRequest;
import com.google.datastore.v1beta3.LookupResponse;
import com.google.datastore.v1beta3.RollbackRequest;
import com.google.datastore.v1beta3.RollbackResponse;
import com.google.datastore.v1beta3.RunQueryRequest;
import com.google.datastore.v1beta3.RunQueryResponse;
import com.google.datastore.v1beta3.client.Datastore;
import com.google.datastore.v1beta3.client.DatastoreException;
import com.google.datastore.v1beta3.client.DatastoreFactory;
import com.google.datastore.v1beta3.client.DatastoreHelper;
import com.google.datastore.v1beta3.client.DatastoreOptions;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {@link CloudDatastoreV1Proxy} that makes remote calls (currently over HTTP).
 *
 * <p>Methods in this class do not populate the project id field in outgoing requests since it
 * is not required when using the API over HTTP.
 *
 * <p>This class is used if (and only if) the {@code DATASTORE_USE_CLOUD_DATASTORE},
 * {@code DATASTORE_PROJECT_ID}, or {@code DATASTORE_APP_ID} environment variable is set.
 *
 * <p>This class is designed to run outside of App Engine in an environment where the app id is
 * potentially unknown. The user has several ways to specify it:
 * <ul>
 * <li>Install the Remote API. The Remote API can retrieve the app id by making a call to the
 * server.
 * <li>Specify the {@code DATASTORE_APP_ID} environment variable. If the project id has also been
 * specified, then the value of {@code DATASTORE_APP_ID} is required to match.
 * <li>Explicitly opt to use the project id instead by setting
 * {@code DATASTORE_USE_PROJECT_ID_AS_APP_ID=true}. This changes the serialized form of entities,
 * so integration with services such as memcache will not work correctly.
 * </ul>
 *
 * <p>{@code DATASTORE_APP_ID} and {@code DATASTORE_USE_PROJECT_ID_AS_APP_ID} cannot both be
 * specified.
 *
 * <p>Separately, {@code DATASTORE_ADDITIONAL_APP_IDS} can be set to a comma-separated list of
 * app ids in order to support foreign keys.
 */
final class RemoteCloudDatastoreV1Proxy implements CloudDatastoreV1Proxy {

  private static final Logger logger =
      Logger.getLogger(RemoteCloudDatastoreV1Proxy.class.getName());

  private static final String URL_OVERRIDE_ENV_VAR = "__DATASTORE_URL_OVERRIDE";

  static final String ADDITIONAL_APP_IDS_VAR = "DATASTORE_ADDITIONAL_APP_IDS";
  private static final String USE_PROJECT_ID_AS_APP_ID_VAR =
      "DATASTORE_USE_PROJECT_ID_AS_APP_ID";
  static final String APP_ID_VAR = "DATASTORE_APP_ID";
  private static final String ALLOW_TRANSACTIONAL_TASKS = "__DATASTORE_ALLOW_TRANSACTIONAL_TASKS";

  private static final ExecutorService executor = Executors.newCachedThreadPool();

  private final Datastore datastore;

  RemoteCloudDatastoreV1Proxy(Datastore datastore) {
    this.datastore = checkNotNull(datastore);
  }

  /**
   * Creates a {@link RemoteCloudDatastoreV1Proxy}. This method has the side effect
   * of installing minimal stubs ({@link EnvironmentFactory} and
   * {@link Delegate}) in the API proxy if they have not already been installed.
   */
  static RemoteCloudDatastoreV1Proxy create(DatastoreServiceConfig config) {
    checkArgument(config.getApiVersion() == ApiVersion.CLOUD_DATASTORE_V1_REMOTE);
    String projectId = getProjectId();
    DatastoreOptions options;
    try {
      options = getDatastoreOptions(projectId);
    } catch (GeneralSecurityException | IOException e) {
      throw new RuntimeException(
          "Could not get Cloud Datastore options from environment.", e);
    }
    ensureApiProxyIsConfigured(projectId);
    populateAdditionalAppIdsMap();
    return new RemoteCloudDatastoreV1Proxy(DatastoreFactory.get().create(options));
  }

  private static void populateAdditionalAppIdsMap() {
    String appIdsVar = EnvProxy.getenv(ADDITIONAL_APP_IDS_VAR);
    if (appIdsVar == null) {
      return;
    }
    String[] appIds = appIdsVar.split(",");
    Map<String, String> projectIdToAppId = new HashMap<>();
    for (String appId : appIds) {
      appId = appId.trim();
      if (!appId.isEmpty()) {
        projectIdToAppId.put(DatastoreApiHelper.toProjectId(appId), appId);
      }
    }
    ApiProxy.getCurrentEnvironment().getAttributes()
        .put(DataTypeTranslator.ADDITIONAL_APP_IDS_MAP_ATTRIBUTE_KEY, projectIdToAppId);
  }

  @Override
  public Future<BeginTransactionResponse> beginTransaction(final BeginTransactionRequest req) {
    return makeCall(new Callable<BeginTransactionResponse>() {
      @Override
      public BeginTransactionResponse call() throws DatastoreException {
        return datastore.beginTransaction(req);
      }
    }, Method.BeginTransaction);
  }

  @Override
  public Future<RollbackResponse> rollback(final RollbackRequest req) {
    return makeCall(new Callable<RollbackResponse>() {
      @Override
      public RollbackResponse call() throws DatastoreException {
        return datastore.rollback(req);
      }
    }, Method.Rollback);
  }

  @Override
  public Future<RunQueryResponse> runQuery(final RunQueryRequest req) {
    return makeCall(new Callable<RunQueryResponse>() {
      @Override
      public RunQueryResponse call() throws DatastoreException {
        return datastore.runQuery(req);
      }
    }, Method.RunQuery);
  }

  @Override
  public Future<LookupResponse> lookup(final LookupRequest req) {
    return makeCall(new Callable<LookupResponse>() {
      @Override
      public LookupResponse call() throws DatastoreException {
        return datastore.lookup(req);
      }
    }, Method.Lookup);
  }

  @Override
  public Future<AllocateIdsResponse> allocateIds(final AllocateIdsRequest req) {
    return makeCall(new Callable<AllocateIdsResponse>() {
      @Override
      public AllocateIdsResponse call() throws DatastoreException {
        return datastore.allocateIds(req);
      }
    }, Method.AllocateIds);
  }

  @Override
  public Future<CommitResponse> commit(final CommitRequest req) {
    return makeCall(new Callable<CommitResponse>() {
      @Override
      public CommitResponse call() throws DatastoreException {
        return datastore.commit(req);
      }
    }, Method.Commit);
  }

  @Override
  public Future<CommitResponse> rawCommit(byte[] bytes) {
    try {
      return commit(CommitRequest.parseFrom(bytes));
    } catch (InvalidProtocolBufferException e) {
      throw new IllegalStateException(e);
    }
  }

  private static <T extends Message> Future<T> makeCall(final Callable<T> request,
      final Method method) {
    return executor.submit(new Callable<T>() {
      @Override
      public T call() throws Exception {
        try {
          return request.call();
        } catch (DatastoreException e) {
          throw DatastoreApiHelper.createV1Exception(e.getCode(), e.getMessage(), method);
        }
      }
    });
  }

  private static DatastoreOptions getDatastoreOptions(String projectId)
      throws GeneralSecurityException, IOException {
    DatastoreOptions.Builder options = new DatastoreOptions.Builder();
    setProjectEndpoint(projectId, options);
    options.credential(getCredential());
    return options.build();
  }

  private static Credential getCredential() throws GeneralSecurityException, IOException {
    if (Boolean.valueOf(EnvProxy.getenv("__DATASTORE_USE_STUB_CREDENTIAL_FOR_TEST"))) {
      return null;
    } else if (EnvProxy.getenv(LOCAL_HOST_ENV_VAR) != null) {
      logger.log(Level.INFO, "{0} environment variable was set. Not using credentials.",
          new Object[] {LOCAL_HOST_ENV_VAR});
      return null;
    }
    String serviceAccount = EnvProxy.getenv(SERVICE_ACCOUNT_ENV_VAR);
    String privateKeyFile = EnvProxy.getenv(PRIVATE_KEY_FILE_ENV_VAR);
    if (serviceAccount != null && privateKeyFile != null) {
      logger.log(Level.INFO, "{0} and {1} environment variables were set. "
          + "Using service account credential.",
          new Object[] {SERVICE_ACCOUNT_ENV_VAR, PRIVATE_KEY_FILE_ENV_VAR});
      return getServiceAccountCredential(serviceAccount, privateKeyFile);
    }
    return GoogleCredential.getApplicationDefault()
        .createScoped(DatastoreOptions.SCOPES);
  }

  private static void setProjectEndpoint(String projectId, DatastoreOptions.Builder options) {
    if (EnvProxy.getenv("DATASTORE_HOST") != null) {
      logger.warning(String.format(
          "Ignoring value of environment variable DATASTORE_HOST. "
          + "To point datastore to a host running locally, use "
          + "the environment variable %s.",
          LOCAL_HOST_ENV_VAR));
    }
    if (EnvProxy.getenv(URL_OVERRIDE_ENV_VAR) != null) {
      options.projectEndpoint(String.format("%s/projects/%s",
          EnvProxy.getenv(URL_OVERRIDE_ENV_VAR), projectId));
      return;
    }
    if (EnvProxy.getenv(LOCAL_HOST_ENV_VAR) != null) {
      options.projectId(projectId);
      options.localHost(EnvProxy.getenv(LOCAL_HOST_ENV_VAR));
      return;
    }
    options.projectId(projectId);
    return;
  }

  private static String getProjectId() {
    String projectIdFromEnv = EnvProxy.getenv(PROJECT_ID_ENV_VAR);
    if (projectIdFromEnv != null) {
      return projectIdFromEnv;
    }
    String appIdFromEnv = EnvProxy.getenv(APP_ID_VAR);
    if (appIdFromEnv != null) {
      return DatastoreApiHelper.toProjectId(appIdFromEnv);
    }
    String projectIdFromComputeEngine = DatastoreHelper.getProjectIdFromComputeEngine();
    if (projectIdFromComputeEngine != null) {
      return projectIdFromComputeEngine;
    }
    throw new IllegalStateException(String.format("Could not determine project ID."
        + " If you are not running on Compute Engine, set the "
        + " %s environment variable.", PROJECT_ID_ENV_VAR));
  }

  private static Credential getServiceAccountCredential(String account, String privateKeyFile)
      throws GeneralSecurityException, IOException {
    return new GoogleCredential.Builder()
        .setTransport(GoogleNetHttpTransport.newTrustedTransport())
        .setJsonFactory(new JacksonFactory())
        .setServiceAccountId(account)
        .setServiceAccountScopes(DatastoreOptions.SCOPES)
        .setServiceAccountPrivateKeyFromP12File(new File(privateKeyFile))
        .build();
  }

  /**
   * Make sure that the API proxy has been configured. If it's already
   * configured (e.g. because the Remote API has been installed or the factory
   * has already been used), do nothing. Otherwise, install a stub environment
   * and delegate.
   */
  private static synchronized void ensureApiProxyIsConfigured(String projectId) {
    boolean hasEnvironmentOrFactory = (ApiProxy.getCurrentEnvironment() != null);
    boolean hasDelegate = (ApiProxy.getDelegate() != null);

    if (hasEnvironmentOrFactory && hasDelegate) {
      boolean allowTransactionalTasks =
          Boolean.valueOf(EnvProxy.getenv(ALLOW_TRANSACTIONAL_TASKS));
      if (!allowTransactionalTasks
          && !(ApiProxy.getDelegate() instanceof TransactionalTaskDisallowingApiProxyDelegate)) {
        @SuppressWarnings("unchecked")
        Delegate<Environment> originalDelegate = ApiProxy.getDelegate();
        ApiProxy.setDelegate(new TransactionalTaskDisallowingApiProxyDelegate(originalDelegate));
      }
      return;
    }

    if (hasEnvironmentOrFactory) {
      throw new IllegalStateException(
          "An ApiProxy.Environment or ApiProxy.EnvironmentFactory was already installed. "
          + "Cannot use Cloud Datastore.");
    } else if (hasDelegate) {
      throw new IllegalStateException(
          "An ApiProxy.Delegate was already installed. Cannot use Cloud Datastore.");
    }

    String appId = EnvProxy.getenv(APP_ID_VAR);
    boolean useProjectIdAsAppId =
        Boolean.valueOf(EnvProxy.getenv(USE_PROJECT_ID_AS_APP_ID_VAR));

    if (appId == null && !useProjectIdAsAppId) {
      throw new IllegalStateException(String.format(
          "Could not not determine app id. To use project id (%s) instead, set "
          + "%s=true. This will affect the serialized form "
          + "of entities and should not be used if serialized entities will be shared between "
          + "code running on App Engine and code running off App Engine. Alternatively, set "
          + "%s=<app id>.",
          projectId, USE_PROJECT_ID_AS_APP_ID_VAR, APP_ID_VAR));
    } else if (appId != null) {
      if (useProjectIdAsAppId) {
        throw new IllegalStateException(String.format(
            "App id was provided (%s) but %s was set to true. "
            + "Please unset either %s or %s.",
            appId, USE_PROJECT_ID_AS_APP_ID_VAR, APP_ID_VAR, USE_PROJECT_ID_AS_APP_ID_VAR));
      } else if (!DatastoreApiHelper.toProjectId(appId).equals(projectId)) {
        throw new IllegalStateException(String.format(
            "App id \"%s\" does not match project id \"%s\".",
            appId, projectId));
      }
    }

    ApiProxy.setEnvironmentFactory(new StubApiProxyEnvironmentFactory(
        useProjectIdAsAppId ? projectId : appId));
    ApiProxy.setDelegate(new StubApiProxyDelegate());
  }

  /**
   * A {@link Delegate} that disallows transactional tasks. Requests not
   * containing a transactional task are delegated to another {@link Delegate}.
   */
  static class TransactionalTaskDisallowingApiProxyDelegate implements Delegate<Environment> {
    static final String NOT_SUPPORTED_MESSAGE =
        "Transactional tasks are not supported under this configuration.";
    private final Delegate<Environment> delegate;

    public TransactionalTaskDisallowingApiProxyDelegate(Delegate<Environment> delegate) {
      this.delegate = delegate;
    }

    @Override
    public byte[] makeSyncCall(Environment environment, String packageName,
        String methodName, byte[] request) throws ApiProxyException {
      checkAllowed(packageName, methodName, request);
      return delegate.makeSyncCall(environment, packageName, methodName, request);
    }

    @Override
    public Future<byte[]> makeAsyncCall(Environment environment, String packageName,
        String methodName, byte[] request, ApiConfig apiConfig) {
      checkAllowed(packageName, methodName, request);
      return delegate.makeAsyncCall(environment, packageName, methodName, request, apiConfig);
    }

    @Override
    public void log(Environment environment, LogRecord record) {
      delegate.log(environment, record);
    }

    @Override
    public void flushLogs(Environment environment) {
      delegate.flushLogs(environment);
    }

    @Override
    public List<Thread> getRequestThreads(Environment environment) {
      return delegate.getRequestThreads(environment);
    }

    private static void checkAllowed(String packageName, String methodName,
        byte[] requestBytes) {
      if (!"taskqueue".equals(packageName)) {
        return;
      }

      try {
        if ("Add".equals(methodName)) {
          checkNonTransactional(TaskQueueAddRequest.parser().parseFrom(requestBytes));
        } else if ("BulkAdd".equals(methodName)) {
          TaskQueueBulkAddRequest req = TaskQueueBulkAddRequest.parser().parseFrom(requestBytes);
          for (TaskQueueAddRequest subReq : req.addRequests()) {
            checkNonTransactional(subReq);
          }
        }
      } catch (InvalidProtocolBufferException e) {
        throw new IllegalArgumentException(e);
      }
    }

    private static void checkNonTransactional(TaskQueueAddRequest req) {
      if (req.hasTransaction() || req.hasDatastoreTransaction()) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_MESSAGE);
      }
    }
  }

  /**
   * A {@link Delegate} that throws {@link UnsupportedOperationException} for
   * all methods.
   */
  static class StubApiProxyDelegate implements Delegate<Environment> {
    private static final String UNSUPPORTED_API_PATTERN =
        "Calls to %s.%s are not supported under this configuration, only "
        + "calls to Cloud Datastore. To use other APIs, first install the "
        + "Remote API.";

    @Override
    public byte[] makeSyncCall(Environment environment, String packageName,
        String methodName, byte[] request) throws ApiProxyException {
      throw new UnsupportedOperationException(
          String.format(UNSUPPORTED_API_PATTERN, packageName, methodName));
    }

    @Override
    public Future<byte[]> makeAsyncCall(Environment environment, String packageName,
        String methodName, byte[] request, ApiConfig apiConfig) {
      throw new UnsupportedOperationException(
          String.format(UNSUPPORTED_API_PATTERN, packageName, methodName));
    }

    @Override
    public void log(Environment environment, LogRecord record) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void flushLogs(Environment environment) {
      throw new UnsupportedOperationException();
    }

    @Override
    public List<Thread> getRequestThreads(Environment environment) {
      throw new UnsupportedOperationException();
    }
  }

  /**
   * An {@link EnvironmentFactory} that builds {@link StubApiProxyEnvironment}s.
   */
  static class StubApiProxyEnvironmentFactory implements EnvironmentFactory {
    private final String appId;

    public StubApiProxyEnvironmentFactory(String appId) {
      this.appId = appId;
    }

    @Override
    public Environment newEnvironment() {
      return new StubApiProxyEnvironment(appId);
    }
  }

  /**
   * An {@link Environment} that supports the minimal subset of features needed
   * to run code from the datastore package outside of App Engine. All other
   * methods throw {@link UnsupportedOperationException}.
   */
  static class StubApiProxyEnvironment implements Environment {
    private final Map<String, Object> attributes;
    private final String appId;

    public StubApiProxyEnvironment(String appId) {
      this.attributes = new HashMap<>();
      this.appId = appId;
    }

    @Override
    public boolean isLoggedIn() {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAdmin() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getVersionId() {
      throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public String getRequestNamespace() {
      throw new UnsupportedOperationException();
    }

    @Override
    public long getRemainingMillis() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getModuleId() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getEmail() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getAuthDomain() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, Object> getAttributes() {
      return attributes;
    }

    @Override
    public String getAppId() {
      return appId;
    }
  }
}
