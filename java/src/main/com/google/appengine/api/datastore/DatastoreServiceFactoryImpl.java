// Copyright 2012 Google Inc. All rights reserved.

package com.google.appengine.api.datastore;

import com.google.appengine.api.datastore.DatastoreServiceConfig.ApiVersion;
import com.google.common.base.Preconditions;

/**
 * Creates DatastoreService instances.
 *
 */
final class DatastoreServiceFactoryImpl implements IDatastoreServiceFactory {

  private static final String MIXED_SERVICES_MESSAGE =
      "Cannot create both Cloud and non-Cloud Datastore services.";

  private static boolean constructedNonCloudService;
  private static boolean constructedCloudService;

  @Override
  public DatastoreService getDatastoreService(DatastoreServiceConfig config) {
    return new DatastoreServiceImpl(getAsyncDatastoreService(config));
  }

  @Override
  public AsyncDatastoreServiceInternal getAsyncDatastoreService(DatastoreServiceConfig config) {
    TransactionStack txnStack = new TransactionStackImpl();
    CloudDatastoreV1Proxy cloudDatastoreProxy = config.getCloudDatastoreV1Proxy();

    ApiVersion apiVersion = config.getApiVersion();

    synchronized (DatastoreServiceFactoryImpl.class) {
      if (apiVersion.isCloudService()) {
        if (constructedNonCloudService) {
          throw new IllegalArgumentException(MIXED_SERVICES_MESSAGE);
        }
        constructedCloudService = true;
      } else {
        if (constructedCloudService) {
          throw new IllegalArgumentException(MIXED_SERVICES_MESSAGE);
        }
        constructedNonCloudService = true;
      }
    }

    switch (apiVersion) {
      case V3:
        Preconditions.checkState(cloudDatastoreProxy == null);
        return new AsyncDatastoreServiceImpl(config, config.constructApiConfig(), txnStack);
      case CLOUD_DATASTORE_V1_VIA_API_PROXY:
        Preconditions.checkState(cloudDatastoreProxy == null);
        return new AsyncCloudDatastoreV1ServiceImpl(config,
            new LocalCloudDatastoreV1Proxy(config.constructApiConfig()), txnStack);
      case CLOUD_DATASTORE_V1_REMOTE:
        if (cloudDatastoreProxy == null) {
          cloudDatastoreProxy = RemoteCloudDatastoreV1Proxy.create(config);
        }
        return new AsyncCloudDatastoreV1ServiceImpl(config, cloudDatastoreProxy, txnStack);
      default:
        throw new IllegalArgumentException(
            "Can't instantiate service with API version: " + apiVersion);
    }
  }

  static synchronized void resetConstructionStateForTest() {
    constructedNonCloudService = false;
    constructedCloudService = false;
  }
}
