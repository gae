package com.google.appengine.api.datastore;

import static com.google.appengine.api.datastore.DatastoreApiHelper.getCurrentProjectId;
import static com.google.appengine.api.datastore.DatastoreApiHelper.makeV1AsyncCall;

import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.ApiConfig;
import com.google.datastore.v1beta3.AllocateIdsRequest;
import com.google.datastore.v1beta3.AllocateIdsResponse;
import com.google.datastore.v1beta3.BeginTransactionRequest;
import com.google.datastore.v1beta3.BeginTransactionResponse;
import com.google.datastore.v1beta3.CommitRequest;
import com.google.datastore.v1beta3.CommitResponse;
import com.google.datastore.v1beta3.Datastore.Method;
import com.google.datastore.v1beta3.LookupRequest;
import com.google.datastore.v1beta3.LookupResponse;
import com.google.datastore.v1beta3.RollbackRequest;
import com.google.datastore.v1beta3.RollbackResponse;
import com.google.datastore.v1beta3.RunQueryRequest;
import com.google.datastore.v1beta3.RunQueryResponse;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.concurrent.Future;

/**
 * Cloud Datastore v1 proxy which forwards RPCs to {@link ApiProxy} via {@link DatastoreApiHelper}.
 * Used when running in an App Engine environment.
 *
 * <p>Methods in this class populate the project id field in outgoing requests by getting it from
 * the current environment.
 */
final class LocalCloudDatastoreV1Proxy implements CloudDatastoreV1Proxy {
  private final ApiConfig apiConfig;

  LocalCloudDatastoreV1Proxy(ApiConfig apiConfig) {
    this.apiConfig = apiConfig;
  }

  @Override
  public Future<BeginTransactionResponse> beginTransaction(BeginTransactionRequest request) {
    return makeV1AsyncCall(
        apiConfig,
        Method.BeginTransaction,
        request.toBuilder().setProjectId(getCurrentProjectId()).build(),
        BeginTransactionResponse.parser());
  }

  @Override
  public Future<RollbackResponse> rollback(RollbackRequest request) {
    return makeV1AsyncCall(
        apiConfig,
        Method.Rollback,
        request.toBuilder().setProjectId(getCurrentProjectId()).build(),
        RollbackResponse.parser());
  }

  @Override
  public Future<RunQueryResponse> runQuery(RunQueryRequest request) {
    return makeV1AsyncCall(
        apiConfig,
        Method.RunQuery,
        request.toBuilder().setProjectId(getCurrentProjectId()).build(),
        RunQueryResponse.parser());
  }

  @Override
  public Future<LookupResponse> lookup(LookupRequest request) {
    return makeV1AsyncCall(
        apiConfig,
        Method.Lookup,
        request.toBuilder().setProjectId(getCurrentProjectId()).build(),
        LookupResponse.parser());
  }

  @Override
  public Future<AllocateIdsResponse> allocateIds(AllocateIdsRequest request) {
    return makeV1AsyncCall(
        apiConfig,
        Method.AllocateIds,
        request.toBuilder().setProjectId(getCurrentProjectId()).build(),
        AllocateIdsResponse.parser());
  }

  @Override
  public Future<CommitResponse> commit(CommitRequest request) {
    return makeV1AsyncCall(
        apiConfig,
        Method.Commit,
        request.toBuilder().setProjectId(getCurrentProjectId()).build(),
        CommitResponse.parser());
  }

  @Override
  public Future<CommitResponse> rawCommit(byte[] request) {
    try {
      return commit(CommitRequest.parseFrom(request));
    } catch (InvalidProtocolBufferException e) {
      throw new IllegalStateException(e);
    }
  }
}
