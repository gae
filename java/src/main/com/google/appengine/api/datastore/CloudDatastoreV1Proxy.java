package com.google.appengine.api.datastore;

import com.google.datastore.v1beta3.AllocateIdsRequest;
import com.google.datastore.v1beta3.AllocateIdsResponse;
import com.google.datastore.v1beta3.BeginTransactionRequest;
import com.google.datastore.v1beta3.BeginTransactionResponse;
import com.google.datastore.v1beta3.CommitRequest;
import com.google.datastore.v1beta3.CommitResponse;
import com.google.datastore.v1beta3.LookupRequest;
import com.google.datastore.v1beta3.LookupResponse;
import com.google.datastore.v1beta3.RollbackRequest;
import com.google.datastore.v1beta3.RollbackResponse;
import com.google.datastore.v1beta3.RunQueryRequest;
import com.google.datastore.v1beta3.RunQueryResponse;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.concurrent.Future;

/**
 * The Cloud Datastore v1 RPC interface. The default implementation,
 * {@link LocalCloudDatastoreV1Proxy}, forwards RPCs to the App Engine API proxy. Other
 * implementations may handle RPCs differently.
 *
 * <p>The use of an interface allows the SDKs to be used in contexts other than the App Engine
 * runtime, without introducing a direct build dependency.
 *
 * <p>Invoking a method sends out the supplied RPC and returns a {@link Future} which clients can
 * block on to retrieve a result.
 */
interface CloudDatastoreV1Proxy {
  Future<BeginTransactionResponse> beginTransaction(BeginTransactionRequest request);
  Future<RollbackResponse> rollback(RollbackRequest request);
  Future<RunQueryResponse> runQuery(RunQueryRequest request);
  Future<LookupResponse> lookup(LookupRequest request);
  Future<AllocateIdsResponse> allocateIds(AllocateIdsRequest request);
  Future<CommitResponse> commit(CommitRequest request);

  /**
   * Equivalent to {@link #commit(CommitRequest)} with a pre-serialized proto. Used by
   * {@link InternalTransactionCloudDatastoreV1} to avoid a second serialization of the proto.
   *
   * @param request byte array which must be deserializable as a {@link CommitRequest}.
   */
  Future<CommitResponse> rawCommit(byte[] request) throws InvalidProtocolBufferException;
}
