package com.google.appengine.tools.development;

/**
 * Provider for implementations of various services within the DevAppServer context.
 *
 * @author emcmanus@google.com (Éamonn McManus)
 */
public interface DevServices {
  DevLogService getLogService();
}
