// Copyright 2010 Google Inc. All Rights Reserved.

package com.google.appengine.tools.remoteapi;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.util.EncodingUtil;

import java.io.IOException;
import java.util.List;

/**
 * Abstract class that handles making HTTP requests to App Engine using
 * cookie-based authentication. The actual mechanism by which the HTTP requests
 * are made is left to subclasses to implement.
 *
 * <p>This class is thread-safe.</p>
 *
 */
abstract class AppEngineClient extends BaseRemoteApiClient {
  private final String userEmail;
  private final Cookie[] authCookies;
  private final int maxResponseSize;

  AppEngineClient(RemoteApiOptions options,
      List<Cookie> authCookies, String appId) {
    super(options, appId);
    if (options == null) {
      throw new IllegalArgumentException("options not set");
    }
    if (authCookies == null) {
      throw new IllegalArgumentException("authCookies not set");
    }
    this.userEmail = options.getUserEmail();
    this.authCookies = authCookies.toArray(new Cookie[0]);
    this.maxResponseSize = options.getMaxHttpResponseSize();
  }

  @Override
  public String serializeCredentials() {
    StringBuilder out = new StringBuilder();
    out.append("host=" + getHostname() + "\n");
    out.append("email=" + userEmail + "\n");
    for (Cookie cookie : authCookies) {
      out.append("cookie=" + cookie.getName() + "=" + cookie.getValue() + "\n");
    }
    return out.toString();
  }

  /**
   * @return the {@link Cookie} objects that should be used for authentication
   */
  Cookie[] getAuthCookies() {
    return authCookies;
  }

  int getMaxResponseSize() {
    return maxResponseSize;
  }

  List<String[]> getHeadersForPost(String mimeType) {
    List<String[]> headers = getHeadersBase();
    headers.add(new String[]{"Content-type", mimeType});
    return headers;
  }

  List<String[]> getHeadersForGet() {
    return getHeadersBase();
  }

  /**
   * Simple class representing an HTTP response.
   */
  static class Response {
    private final int statusCode;
    private final byte[] responseBody;
    private final String responseCharSet;

    Response(int statusCode, byte[] responseBody, String responseCharSet) {
      this.statusCode = statusCode;
      this.responseBody = responseBody;
      this.responseCharSet = responseCharSet;
    }

    int getStatusCode() {
      return statusCode;
    }

    byte[] getBodyAsBytes() {
      return responseBody;
    }

    String getBodyAsString() {
      return EncodingUtil.getString(responseBody, responseCharSet);
    }
  }

  abstract LegacyResponse legacyPost(String path, String mimeType, byte[] body) throws IOException;
  abstract LegacyResponse legacyGet(String path) throws IOException;
}
