package com.google.appengine.tools.remoteapi;

import java.io.IOException;

/**
 * An interface describing a Remote API client. Implementations of this
 * interface must be thread-safe.
 */
interface RemoteApiClient {

  /**
   * @return The path to the Remote API handler.
   */
  String getRemoteApiPath();
  
  /**
   * @return The app id associated with this client, possibly null.
   */
  String getAppId();
  
  /**
   * @return The credentials assocated with this client as a String.
   * Non-cookie based implementations may not support this method.
   */
  String serializeCredentials();

  /**
   * Executes an HTTP POST request against {@code path}.
   */
  AppEngineClient.Response post(String path, String mimeType, byte[] body) throws IOException;
  
  /**
   * Executes an HTTP GET request against {@code path}.
   */
  AppEngineClient.Response get(String path) throws IOException;
}
