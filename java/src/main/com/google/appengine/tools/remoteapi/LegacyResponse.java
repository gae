package com.google.appengine.tools.remoteapi;

import org.apache.commons.httpclient.util.EncodingUtil;

class LegacyResponse {
  private final int statusCode;
  private final byte[] responseBody;
  private final String responseCharSet;

  LegacyResponse(int statusCode, byte[] responseBody, String responseCharSet) {
    this.statusCode = statusCode;
    this.responseBody = responseBody;
    this.responseCharSet = responseCharSet;
  }

  int getStatusCode() {
    return statusCode;
  }

  byte[] getBodyAsBytes() {
    return responseBody;
  }

  String getBodyAsString() {
    return EncodingUtil.getString(responseBody, responseCharSet);
  }
}
