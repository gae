package com.google.appengine.tools.admin;

import com.google.auto.value.AutoValue;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

/**
 * Auto-detects source context that was used to build and deploy an application by scanning
 * its git directory.
 */
final class RepoInfo {
  /**
   * SourceContext is a reference to a persistent snapshot of the source tree stored in a
   * version control repository.
   */
  @AutoValue
  abstract static class SourceContext {
    /** A URL string identifying the repository. */
    @Nullable
    abstract String getRepositoryUrl();
    /** The canonical, unique, and persistent identifier of the deployed revision. */
    abstract String getRevisionId();
    /** The source context message in JSON format.*/
    @Nullable
    abstract String getJson();
    /** The cloud repo project id, if available. */
    @Nullable
    abstract String getProjectId();
    /** The cloud repo id, if available. */
    @Nullable
    abstract String getRepoId();
    /** The cloud repo name, if available. */
    @Nullable
    abstract String getRepoName();

    boolean isCloudRepo() {
      return getRepoId() != null || getProjectId() != null;
    }

    private static final Pattern CLOUD_REPO_RE = Pattern.compile(
          "^https://"
          + "(?<hostname>[^/]*)/"
          + "(?<idtype>p|id)/"
          + "(?<projectOrRepoId>[^/?#]+)"
          + "(/r/(?<repoName>[^/?#]+))?"
          + "([/#?].*)?");

    static SourceContext createLocal(String revisionId) {
      return new AutoValue_RepoInfo_SourceContext(null, revisionId, null, null, null, null);
    }

    /**
     * Builds the source context from a URL and revision ID.
     *
     * <p>If {@code repoUrl} conforms to the predefined format of Google repo URLs, it parses out
     * the components of a Source API CloudRepoSourceContext. If {@code repoUrl} is not a valid
     * Google repo URL, it is treated as a generic GitSourceContext URL. The function assembles
     * everything into a JSON string. JSON values are escaped.
     *
     * <p>It would be better to use some JSON library to build JSON string (like gson). We craft
     * the JSON string manually to avoid new dependencies for the SDK.
     *
     * @param repoUrl remote git URL corresponding to Google Cloud repo
     * @param revision the HEAD revision of the current branch
     * @return source context BLOB serialized as JSON string, or null if we fail to
     *         parse {@code repoUrl}
     */
    static SourceContext createFromUrl(@Nullable String repoUrl, String revisionId) {
      if (repoUrl == null) {
        return createLocal(revisionId);
      }

      Matcher match =  CLOUD_REPO_RE.matcher(repoUrl);
      if (match.matches()) {
        String idType = match.group("idtype");
        if ("id".equals(idType)) {
          String rawRepoId = match.group("projectOrRepoId");
          if (!Strings.isNullOrEmpty(rawRepoId)
              && Strings.isNullOrEmpty(match.group("repoName"))) {
            return SourceContext.createFromRepoId(repoUrl, revisionId, rawRepoId);
          }
        } else if ("p".equals(idType)) {
          String projectId = match.group("projectOrRepoId");
          if (!Strings.isNullOrEmpty(projectId)) {
            String repoName = match.group("repoName");
            if (Strings.isNullOrEmpty(repoName)) {
              repoName = "default";
            }
            return SourceContext.createFromRepoName(repoUrl, revisionId, projectId, repoName);
          }
        }
      }
      return SourceContext.createGit(repoUrl, revisionId);
    }

    static SourceContext createFromRepoId(String repoUrl, String revisionId, String repoId) {
      String json = String.format(
          "{\"cloudRepo\": {\"repoId\": {\"uid\": \"%s\"}, \"revisionId\": \"%s\"}}",
          Utility.jsonEscape(repoId), Utility.jsonEscape(revisionId));
      return new AutoValue_RepoInfo_SourceContext(repoUrl, revisionId, json, null, repoId, null);
    }

    static SourceContext createFromRepoName(
        String repoUrl, String revisionId, String projectId, String repoName) {
      String jsonRepoId = String.format(
          "{\"projectRepoId\": {\"projectId\": \"%s\", \"repoName\": \"%s\"}}",
          Utility.jsonEscape(projectId), Utility.jsonEscape(repoName));
      String json = String.format("{\"cloudRepo\": {\"repoId\": %s, \"revisionId\": \"%s\"}}",
          jsonRepoId, Utility.jsonEscape(revisionId));
      return new AutoValue_RepoInfo_SourceContext(
          repoUrl, revisionId, json, projectId, null, repoName);
    }

    static SourceContext createGit(String repoUrl, String revisionId) {
      String json = String.format("{\"git\": {\"url\": \"%s\", \"revisionId\": \"%s\"}}",
          Utility.jsonEscape(repoUrl), Utility.jsonEscape(revisionId));
      return new AutoValue_RepoInfo_SourceContext(repoUrl, revisionId, json, null, null, null);
    }
  }

  /**
   * Exception for all problems calling git or parsing its output.
   */
  static final class GitException extends Exception {
    GitException(String message) {
      super(message);
    }

    GitException(String message, Throwable cause) {
      super(message, cause);
    }
  }

  /**
   * Abstraction over calling git for unit tests.
   */
  interface GitClient {
    /**
     * Calls git with the given args.
     *
     * <p>The working directory is set to the deployed target directory. This is the potential
     * git repository directory. The current working directory (i.e. directory from which
     * appcfg is called) is irrelevant.
     *
     * <p>Git might not be used by the developer. In this case {@code baseDir} is not a git
     * repository or git might not be even installed on the system. In these cases this
     * function will throw {@link GitException}.
     *
     * @param args arguments for the git command
     * @return raw output of the git command (stdout, not stderr)
     * @throws GitException if not a git repository or problem calling git
     */
    String callGit(String... args) throws GitException;
  }

  /**
   * Implements {@link GitClient} interface by invoking git command as a separate process.
   */
  private static final class GitCommandClient implements GitClient {
    /**
     * Potential git repo directory (doesn't have to be root repo directory).
     */
    private final File baseDir;

    /**
     * Class constructor.
     *
     * @param baseDir potential git repo directory (doesn't have to be root repo directory)
     */
    GitCommandClient(File baseDir) {
      this.baseDir = baseDir;
    }

    @Override
    public String callGit(String... args) throws GitException {
      ImmutableList<String> command = ImmutableList.<String>builder()
          .add(Utility.isOsWindows() ? "git.exe" : "git")
          .add(args)
          .build();

      try {
        Process process = new ProcessBuilder(command)
            .directory(baseDir)
            .start();

        StringWriter stdOutWriter = new StringWriter();
        Thread stdOutPumpThread =
            new Thread(new OutputPump(process.getInputStream(), new PrintWriter(stdOutWriter)));
        stdOutPumpThread.start();

        StringWriter stdErrWriter = new StringWriter();
        Thread stdErrPumpThread =
            new Thread(new OutputPump(process.getErrorStream(), new PrintWriter(stdErrWriter)));
        stdErrPumpThread.start();

        int rc = process.waitFor();
        stdOutPumpThread.join();
        stdErrPumpThread.join();

        String stdout = stdOutWriter.toString();
        String stderr = stdErrWriter.toString();

        logger.fine(String.format("%s completed with code %d\n%s%s",
            command, rc, stdout, stderr));

        if (rc != 0) {
          throw new GitException(String.format(
              "git command failed (exit code = %d), command: %s\n%s%s",
              rc, command, stdout, stderr));
        }

        return stdout;
      } catch (InterruptedException ex) {
        throw new GitException(String.format(
            "InterruptedException caught while executing git command: %s", command), ex);
      } catch (IOException ex) {
        throw new GitException(String.format("Failed to invoke git: %s", command), ex);
      }
    }
  }

  private static final Logger logger = Logger.getLogger(RepoInfo.class.getName());

  /**
   * Regular expression pattern to capture list of origins for the local repo.
   */
  private static final String REMOTE_URL_PATTERN = "remote\\.(.*)\\.url";

  /**
   * Calls git to obtain information about the repository.
   */
  private final GitClient git;

  /**
   * Class constructor.
   *
   * @param baseDir potential git repo directory (doesn't have to be root repo directory)
   */
  RepoInfo(File baseDir) {
    this(new GitCommandClient(baseDir));
  }

  /**
   * Class constructor.
   *
   * @param git git client interface
   */
  RepoInfo(GitClient git) {
    this.git = git;
  }

  /**
   * Constructs a SourceContext for the HEAD revision.
   *
   * @return Returns null if there is no local revision ID.<p><ul>
   *     <li>If there is exactly one remote repo associated with the local repo, its context will be
   *     returned.
   *     <li>If there is exactly one Google-hosted remote repo associated with the local repo, its
   *     {@code SourceContext} will be returned, even if there other non-Google remote repos
   *     associated with the local repo.
   *     </ul><p>In all other cases, the return value will contain only the local head revision ID.
   */
  @Nullable
  SourceContext getSourceContext() {
    Multimap<String, String> remoteUrls;
    String revision = null;

    try {
      revision = getGitHeadRevision();

      remoteUrls = getGitRemoteUrls();
      if (remoteUrls.isEmpty()) {
        logger.fine("Local git repo has no remote URLs");
        return SourceContext.createLocal(revision);
      }

    } catch (GitException e) {
      logger.fine("not a git repository or problem calling git");
      return revision == null ? null : SourceContext.createLocal(revision);
    }

    SourceContext bestReturn = null;
    boolean hasAmbiguousRepos = false;
    for (Map.Entry<String, String> remoteUrl : remoteUrls.entries()) {
      SourceContext candidate = SourceContext.createFromUrl(remoteUrl.getValue(), revision);
      if (candidate.isCloudRepo()) {
        if (bestReturn != null && bestReturn.isCloudRepo()
            && !bestReturn.getJson().equals(candidate.getJson())) {
          logger.fine("Ambiguous Google Cloud Repository in the remote URLs");
          return SourceContext.createLocal(revision);
        }
        bestReturn = candidate;
      } else if (bestReturn == null) {
        bestReturn = candidate;
      } else if (!bestReturn.isCloudRepo()) {
        hasAmbiguousRepos = true;
      }
    }

    if (bestReturn != null && !bestReturn.isCloudRepo() && hasAmbiguousRepos) {
      logger.fine("Remote URLs include multiple non-Google repos and no Google repo.");
      return SourceContext.createLocal(revision);
    }

    return bestReturn;
  }

  /**
   * Calls git to print every configured remote URL.
   *
   * @return raw output of the command
   * @throws GitException if not a git repository or problem calling git
   */
  private String getGitRemoteUrlConfigs() throws GitException {
    return git.callGit("config", "--get-regexp", REMOTE_URL_PATTERN);
  }

  /**
   * Finds the list of git remotes for the given source directory.
   *
   * @return A list of remote name to remote URL mappings, empty if no remotes are found
   * @throws GitException if not a git repository or problem calling git
   */
  private Multimap<String, String> getGitRemoteUrls() throws GitException {
    String remoteUrlConfigOutput = getGitRemoteUrlConfigs();
    if (remoteUrlConfigOutput.isEmpty()) {
      return ImmutableMultimap.of();
    }

    Pattern remoteUrlRe = Pattern.compile(REMOTE_URL_PATTERN);

    ImmutableMultimap.Builder<String, String> result = ImmutableMultimap.builder();

    String[] configLines = remoteUrlConfigOutput.split("\\r?\\n");
    for (String configLine : configLines) {
      if (configLine.isEmpty()) {
        continue;
      }

      String[] parts = configLine.split(" +");
      if (parts.length != 2) {
        logger.fine(String.format("Skipping unexpected git config line, incorrect segments: %s",
            configLine));
        continue;
      }

      String remoteUrlConfigName = parts[0];
      String remoteUrl = parts[1];

      Matcher matcher = remoteUrlRe.matcher(remoteUrlConfigName);
      if (!matcher.matches()) {
        logger.fine(String.format("Skipping unexpected git config line, could not match remote: %s",
            configLine));
        continue;
      }

      String remoteUrlName = matcher.group(1);

      result.put(remoteUrlName, remoteUrl);
    }

    logger.fine(String.format("Remote git URLs: %s", result.toString()));

    return result.build();
  }

  /**
   * Finds the current HEAD revision for the given source directory
   *
   * @return the HEAD revision of the current branch
   * @throws GitException if not a git repository or problem calling git
   */
  private String getGitHeadRevision() throws GitException {
    String head = git.callGit("rev-parse", "HEAD").trim();
    if (head.isEmpty()) {
      throw new GitException("Empty head revision returned by git");
    }

    return head;
  }
}
