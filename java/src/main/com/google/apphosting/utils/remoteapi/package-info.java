// Copyright 2010 Google Inc.  All rights reserved.

/**
 * The {@code com.google.apphosting.utils.remoteapi} package contains
 * a servlet that implements the server portion of Google App Engine's
 * Remote API functionality, which allows client code to execute
 * arbitray API calls on a remote App Engine application.
 */
package com.google.apphosting.utils.remoteapi;
