package com.google.apphosting.utils.config;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import org.mortbay.xml.XmlParser;
import org.mortbay.xml.XmlParser.Node;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

/**
 * Utility functions for processing XML.
 */
public class XmlUtils {
  private static final Logger logger = Logger.getLogger(XmlUtils.class.getName());

  static String getText(XmlParser.Node node) throws AppEngineConfigException{
    Object child = node.get(0);
    String value;
    if (child == null) {
      value = "";
    } else {
      if (!(child instanceof String)) {
        String msg = "Invalid XML: String content expected in node '" + node.getTag() + "'.";
        logger.log(Level.SEVERE, msg);
        throw new AppEngineConfigException(msg);
      }
      value = (String) child;
    }

    return value.trim();
  }

  /**
   * Parses the input stream and returns the {@link Node} for the root element.
   *
   * @throws AppEngineConfigException If the input stream cannot be parsed.
   */
  static Node parse(InputStream is) {
    XmlParser xmlParser = new XmlParser();
    try {
      return xmlParser.parse(is);
    } catch (IOException e) {
      String msg = "Received IOException parsing the input stream.";
      logger.log(Level.SEVERE, msg, e);
      throw new AppEngineConfigException(msg, e);
    } catch (SAXException e) {
      String msg = "Received SAXException parsing the input stream.";
      logger.log(Level.SEVERE, msg, e);
      throw new AppEngineConfigException(msg, e);
    }
  }

  /**
   * Validates a given XML document against a given schema.
   *
   * @param xmlFilename filename with XML document.
   * @param schema XSD schema to validate with.
   *
   * @throws AppEngineConfigException for malformed XML, or IO errors
   */
  public static void validateXml(String xmlFilename, File schema) {
    File xml = new File(xmlFilename);
    if (!xml.exists()) {
      throw new AppEngineConfigException("Xml file: " + xml.getPath() + " does not exist.");
    }
    if (!schema.exists()) {
      throw new AppEngineConfigException("Schema file: " + schema.getPath() + " does not exist.");
    }
    try {
      validateXmlContent(Files.toString(xml, Charsets.UTF_8), schema);
    } catch (IOException ex) {
      throw new AppEngineConfigException(
          "IO error validating " + xmlFilename + " against " + schema.getPath(), ex);
    }
  }

  /**
   * Validates a given XML document against a given schema.
   *
   * @param content a String containing the entire XML to validate.
   * @param schema XSD schema to validate with.
   *
   * @throws AppEngineConfigException for malformed XML, or IO errors
   */
  public static void validateXmlContent(String content, File schema) {
    if (!schema.exists()) {
      throw new AppEngineConfigException("Schema file: " + schema.getPath() + " does not exist.");
    }
    try {
      SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      try {
        factory
            .newSchema(schema)
            .newValidator()
            .validate(new StreamSource(new ByteArrayInputStream(content.getBytes(Charsets.UTF_8))));
      } catch (SAXException ex) {
        throw new AppEngineConfigException(
            "XML error validating " + content + " against " + schema.getPath(), ex);
      }
    } catch (IOException ex) {
      throw new AppEngineConfigException(
          "IO error validating " + content + " against " + schema.getPath(), ex);
    }
  }
}
