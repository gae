package com.google.apphosting.utils.config;

import org.mortbay.xml.XmlParser.Node;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.util.Stack;
import java.util.logging.Level;

/**
 * Creates an {@link IndexesXml} instance from
 * WEB-INF/datastore-indexes.xml.  If you want to read the
 * configuration from a different file, subclass and override
 * {@link #getFilename()}.  If you want to read the configuration from
 * something that isn't a file, subclass and override
 * {@link #getInputStream()}.
 *
 * This class performs some validation on the XML, but it does rely on
 * the fact that (by the time our readIndexesXml() method is called)
 * the XML has already been partially validated by the XML Schema:
 * <pre>
 *     java/com/google/appengine/tools/development/datastore-indexes.xsd
 * </pre>
 */
public class IndexesXmlReader extends AbstractConfigXmlReader<IndexesXml> {

  /**
   * Relative-to-{@code GenerationDirectory.GENERATED_DIR_PROPERTY} file for
   * generated index.
   */
  public static final String GENERATED_INDEX_FILENAME = "datastore-indexes-auto.xml";
  public static final String INDEX_FILENAME = "datastore-indexes.xml";
  public static final String INDEX_YAML_FILENAME = "WEB-INF/index.yaml";

  /** Name of the XML tag in {@code datastore-indexes.xml} for autoindexing */
  public static final String AUTOINDEX_TAG = "autoGenerate";

  private static final String FILENAME = "WEB-INF/datastore-indexes.xml";

  private static final String INDEX_TAG = "datastore-index";

  public static final String KIND_PROP = "kind";
  public static final String ANCESTOR_PROP = "ancestor";
  public static final String PROPERTY_TAG = "property";
  public static final String NAME_PROP = "name";
  public static final String DIRECTION_PROP = "direction";
  public static final String MODE_PROP = "mode";

  private IndexesXml indexesXml;

  /**
   * Constructs a reader for the {@code indexes.xml} configuration of a given app.
   * @param appDir root directory of the application
   */
  public IndexesXmlReader(String appDir) {
    super(appDir, false);
  }

  /**
   * Reads the configuration file.
   * @return an {@link IndexesXml} representing the parsed configuration.
   */
  public IndexesXml readIndexesXml() {
    return readConfigXml();
  }

  /**
   * Reads the index configuration.  If neither the user-written nor the
   * auto-generated config file exists, returns a {@code null}.  Otherwise,
   * reads both files (if available) and returns the union of both sets of
   * indexes.
   *
   * @throws AppEngineConfigException If the file cannot be parsed properly
   */
  @Override
  protected IndexesXml readConfigXml() {
    InputStream is = null;
    String filename = null;

    indexesXml = new IndexesXml();
    try {
      if (fileExists()) {
        filename = getFilename();
        is = getInputStream();
        processXml(is);
        logger.info("Successfully processed " + filename);
      }
      if (yamlFileExists()) {
        filename = getYamlFilename();
        IndexYamlReader.parse(getYamlReader(), indexesXml);
        logger.info("Successfully processed " + filename);
      }
      if (generatedFileExists()) {
        filename = getAutoFilename();
        is = getGeneratedStream();
        processXml(is);
        logger.info("Successfully processed " + filename);
      }
    } catch (Exception e) {
      String msg = "Received exception processing " + filename;
      logger.log(Level.SEVERE, msg, e);
      if (e instanceof AppEngineConfigException) {
        throw (AppEngineConfigException) e;
      }
      throw new AppEngineConfigException(msg, e);
    } finally {
      close(is);
    }
    return indexesXml;
  }

  @Override
  protected IndexesXml processXml(InputStream is) {
    parse(new ParserCallback() {
      IndexesXml.Index index;
      IndexesXml.Type indexType = null;

      /**
       * Assembles index definitions during XML parsing, and
       * performs additional validation that was not already done by
       * the XML Schema.
       * <p>
       * There are two types of index definition: "ordered" and
       * "geo-spatial".  For an ordered index, an "ancestor"
       * specification is optional, and all "property" elements may
       * optionally specify a "direction", but may not specify a
       * "mode".  In a geo-spatial index, "ancestor" is irrelevant
       * (and therefore disallowed), and property elements may
       * specify a mode, but may not specify a direction.
       */
      @Override
      public void newNode(Node node, Stack<Node> unusedContainingElements) {
        if (INDEX_TAG.equalsIgnoreCase(node.getTag())) {
          String kind = node.getAttribute(KIND_PROP);
          Boolean ancestorProp = null;
          String anc = node.getAttribute(ANCESTOR_PROP);
          if (anc == null) {
            indexType = null;
          } else {
            indexType = IndexesXml.Type.ORDERED;
            ancestorProp = anc.equals("true") || anc.equals("1");
          }
          index = indexesXml.addNewIndex(kind, ancestorProp);
        } else if (PROPERTY_TAG.equalsIgnoreCase(node.getTag())) {
          assert(index != null);
          String name = node.getAttribute(NAME_PROP);
          String direction = node.getAttribute(DIRECTION_PROP);
          String mode = node.getAttribute(MODE_PROP);

          if (direction != null) {
            if (mode != null || indexType == IndexesXml.Type.GEO_SPATIAL) {
              throw new AppEngineConfigException(
                  "The 'direction' attribute may not be specified in a 'geospatial' index.");
            }
            indexType = IndexesXml.Type.ORDERED;
          } else if (mode != null) {
            if (indexType == IndexesXml.Type.ORDERED) {
              throw new AppEngineConfigException(
                  "The 'mode' attribute may not be specified with 'direction' or 'ancestor'.");
            }
            indexType = IndexesXml.Type.GEO_SPATIAL;
          }

          index.addNewProperty(name, direction, mode);
        }
      }
    }, is);
    return indexesXml;
  }

  @Override
  protected String getRelativeFilename() {
    return FILENAME;
  }

  protected File getGeneratedFile() {
    File genFile = new File(GenerationDirectory.getGenerationDirectory(new File(appDir)),
                    GENERATED_INDEX_FILENAME);
    return genFile;
  }

  public String getAutoFilename() {
    return getGeneratedFile().getPath();
  }

  protected boolean generatedFileExists() {
    return getGeneratedFile().exists();
  }

  protected InputStream getGeneratedStream() throws Exception {
    return new FileInputStream(getGeneratedFile());
  }

  protected String getYamlFilename() {
    return appDir + INDEX_YAML_FILENAME;
  }

  protected boolean yamlFileExists() {
    return new File(getYamlFilename()).exists();
  }

  protected Reader getYamlReader() {
    try {
      return new FileReader(getYamlFilename());
    } catch (FileNotFoundException ex) {
      throw new AppEngineConfigException("Cannot find file" + getYamlFilename());
    }
  }
}
