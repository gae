package com.google.apphosting.api;

/**
 * Stores tracing context including IDs and settings.
 */
public class CloudTraceContext {
  private final byte[] traceId;
  private final long spanId;
  private final long traceMask;

  /**
   * Constructs the object based on the given parameters.
   * @param traceId id of the trace.
   * @param spanId current span id.
   * @param traceMask trace options.
   */
  public CloudTraceContext(byte[] traceId, long spanId, long traceMask) {
    this.traceId = traceId;
    this.spanId = spanId;
    this.traceMask = traceMask;
  }

  public byte[] getTraceId() {
    return traceId;
  }

  public long getSpanId() {
    return spanId;
  }

  public long getTraceMask() {
    return traceMask;
  }
}
