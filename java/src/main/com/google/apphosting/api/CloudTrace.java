package com.google.apphosting.api;

import java.util.Map;

import javax.annotation.Nullable;

/**
 * Holds the current trace context visible to user code. If present, this object will be stored
 * under the KEY-variable in an Environment's property.
 *
 * This class is used to share the current trace context between run time and user code. It is not
 * responsible for managing contexts. It only keeps the most current context for a given thread.
 */
public abstract class CloudTrace {
  private static final String KEY = "com.google.apphosting.api.cloudTrace";

  private static final ThreadLocal<CloudTraceContext> perThreadContext = new ThreadLocal<>();

  /**
   * Returns the corresponding CloudTrace object for a given environment. If the
   * environment does not have a CloudTrace object, null will be returned.
   */
  @Nullable
  private static CloudTrace get(ApiProxy.Environment env) {
    return (CloudTrace) env.getAttributes().get(KEY);
  }

  /**
   * Returns the current trace context for the given environment and the current thread.
   * @param env the current environment.
   */
  @Nullable
  public static CloudTraceContext getCurrentContext(ApiProxy.Environment env) {
    CloudTrace cloudTrace = CloudTrace.get(env);
    return (cloudTrace == null) ? null : cloudTrace.getCurrentContext();
  }

  /**
   * Returns the current trace context for the current thread.
   */
  @Nullable
  private CloudTraceContext getCurrentContext() {
    CloudTraceContext context = perThreadContext.get();
    return (context == null) ? getDefaultContext() : context;
  }

  /**
   * Sets the current trace context for the current environment and the current thread.
   * @param env the current environment.
   * @param context the current trace context.
   */
  public static void setCurrentContext(
      ApiProxy.Environment env,
      @Nullable CloudTraceContext context) {
    CloudTrace cloudTrace = CloudTrace.get(env);
    if (cloudTrace != null) {
      perThreadContext.set(context);
    }
  }

  /**
   * Binds this object to a particular environment.
   * @param env the Environment object to bind this object to.
   * @throws IllegalStateException if an object is already bound
   */
  protected void bind(ApiProxy.Environment env) {
    CloudTrace original;
    Map<String, Object> attrs = env.getAttributes();

    synchronized (attrs) {
      original = (CloudTrace) attrs.get(KEY);
      if (original == null) {
        attrs.put(KEY, this);
        return;
      }
    }
    if (original != this) {
      throw new IllegalStateException("Cannot replace existing CloudTrace object");
    }
  }

  /**
   * Returns the default context when a thread-specific one doesn't exist.
   */
  protected abstract CloudTraceContext getDefaultContext();
}
